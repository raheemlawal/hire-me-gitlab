# Hire Me GitLab

Bootstrapping my way to getting a SWE job with GitLab in ~1-2. months.

## Story

With 2024 around the corner, I recently reflected on my life situation and am very grateful. However, I am human and have goals that I am excited about and ready to acheive!

I thought of this idea and believe it will be beneficial because: 

* <u>Skill Development</u> : This will be a chance to build code and add skills.

* <u>Public Execise</u> : It will keep me accountable and showcase lessons learned/mistakes.

* <u>Unique</u> : Nobody is applying like this. Basically the ultimate cover letter.

* <u>Speed</u> : Instead of spending a couple months studying for interviews, I can do this and start right away.

* <u>Rewards</u> : High paying job and globally remote! I want to travel and be able to support myself and others. 

## Plan
    
### Structure

<u>App</u>

1. **Blog web app**: will show my experience with GitLab features and showcase my full-stack development skills.

### Other Features

I will try to get the attention of internal recruiters, hiring managers, and software leads at the company. Hopefully it will become interactive!
