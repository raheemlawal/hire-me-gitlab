const db = require("../models");
const Journal = db.journals;
const Op = db.Sequelize.Op;

// Create and Save a new Journal
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
        return;
      }
    
      // Create a Journal
      const journal = {
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
      };
    
      // Save Journal in the database
      Journal.create(journal)
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the Journal."
          });
        });
};

/*

// Retrieve all Journals from the database.
exports.findAll = (req, res) => {
  
};

// Find a single Journal with an id
exports.findOne = (req, res) => {
  
};

// Update a Journal by the id in the request
exports.update = (req, res) => {
  
};

// Delete a Journal with the specified id in the request
exports.delete = (req, res) => {
  
};

// Delete all Journals from the database.
exports.deleteAll = (req, res) => {
  
};

// Find all published Journals
exports.findAllPublished = (req, res) => {
  
};

*/